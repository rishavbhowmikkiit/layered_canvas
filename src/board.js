import {canvas} from './canvas.js'
import { svg_canvas, SVGCanvas } from './svgcanvas.js'

class Board{
    /** @type {HTMLDivElement} */
    #element
    /** @type {boolean} */
    #svg_cnv
    /** @type {[SVGCanvas]} */
    #layers = []

    /**
     * @param {HTMLDivElement} div 
     * @param {boolean} svg_cnv 
     */
    constructor(div, svg_cnv = true){
        if(div.tagName !== 'DIV') throw Error("Invalid Element type")
        //set some attribute
        {
            div.style["touchAction"] = 'none'
            div.style["outline"] = '0.5px solid black'
            div.style["background-color"] = 'transparent'
            //div["height"] = '450'
            //div["width"] = '800'
            div.style["overflow"] = 'hidden'
            div.style["position"] = 'fixed'
            div.style["top"] = '0'
            div.style["left"] = '0'
        }
        this.#element = div
        this.#svg_cnv = true
        this.add_canvas()
    }
    /** @returns {HTMLDivElement} */
    getBoard(){
        return this.#element
    }
    add_canvas(){
        console.log(this.#svg_cnv);
        if(!this.#svg_cnv){
            const cv = canvas();
            this.#element.appendChild(cv.getElement());
            cv.start_stylus_listener()
        }else{
            const scv = svg_canvas(this.#element);
            scv.start_stylus_listener()
            this.#layers.push(scv)
        }
        this.write_all_svg_canvas()
    }
    /**
     * @param {boolean} svg_cnv 
     */
    set_svg_canvas(svg_cnv){
        this.#svg_cnv = svg_cnv
    }
    /**
     * @param {string} url - valid URL for image
     * @param {{x:string, y:string, width:string, height:string}} options
     */
    add_image(url, options){
        const img = document.createElement('img')
        img.style.position = 'fixed'
        this.#element.appendChild(img)
        img.src = url
        img.style.top = options.y? options.y+"px": "0px"
        img.style.left = options.x? options.x+"px": "0px"
        img.style.width = options.width? options.width+"px": "200px"
        img.style.height = options.width? options.height+"px": "auto"
        this.add_canvas()
    }
    add_iframe(url, options){
        const img = document.createElement('iframe')
        img.style.position = 'fixed'
        this.#element.appendChild(img)
        img.src = url
        img.style["background-color"] = 'white'
        img.style.top = options.y? options.y+"px": "0px"
        img.style.left = options.x? options.x+"px": "0px"
        img.style.width = options.width? options.width+"px": "200px"
        img.style.height = options.width? options.height+"px": "auto"
        this.add_canvas()
    }
    /**
     * @param {boolean} b 
     */
    navigate(b){
        console.log(this.getBoard().childNodes);
        if(b){
            this.getBoard().childNodes.forEach(node=>{
                console.log(node.tagName);
                if((node.tagName !== 'CANVAS') && (node.tagName !== 'svg'))
                node.style['z-index'] = 1
            })
        }else{
            this.getBoard().childNodes.forEach(node=>{
                node.style['z-index'] = 0
            })
        }
    }
    select_all_svg_canvas(){
        this.#layers.forEach(l=>{
            l.select_mode()
        })
    }
    write_all_svg_canvas(){
        this.#layers.forEach(l=>{
            l.pause_mode()
        })
        if(this.#layers.length)
            this.#layers[this.#layers.length-1].write_mode()
    }
    pause_all_svg_canvas(){
        this.#layers.forEach(l=>{
            l.pause_mode()
        })
    }
}

/**
 * @param {HTMLElement} element
 * @returns {Board}
 */
function board(element){
    const div = document.createElement('div')
    element.appendChild(div)
    return new Board(div)
}

export {board}