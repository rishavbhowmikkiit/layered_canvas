class Canvas{
    /** @type {HTMLCanvasElement} */
    #canvas_element
    /** @type {boolean} */
    #listen_stylus
    /** @type {{live:boolean}} */
    #stylus_state
    /**
     * @param {HTMLCanvasElement} element - Canvas element
     */
    constructor(element){
        const canvas_element = element
        if(canvas_element.tagName !== 'CANVAS') throw Error("Invalid Element type")
        //set some attribute
        {
            canvas_element.style["touchAction"] = 'none'
            canvas_element.style["outline"] = '0.5px solid black'
            canvas_element.style["background-color"] = 'transparent'
            canvas_element["height"] = '450'
            canvas_element["width"] = '800'
            canvas_element.style["position"] = 'fixed'
            canvas_element.style["top"] = '0'
            canvas_element.style["left"] = '0'
        }
        this.#canvas_element = canvas_element
        
        //console.log(Date.now());
        /*for(var i=0; i<50000; i++){
            this.draw_line({x:Math.random()*500,y:Math.random()*500}, {x:Math.random()*500,y:Math.random()*500})
        }
        console.log(Date.now());*/
    }
    getElement(){
        return this.#canvas_element
    }
    /**
     * @returns {CanvasRenderingContext2D}
     */
    getContext(){
        return this.#canvas_element.getContext("2d")
    }
    /**
     * @param {{x:number, y:number}} from 
     * @param {{x:number, y:number}} to 
     * @param {{color?:string, width?:number}} options
     */
    draw_line(from, to, options = {}){
        const ctx = this.getContext()
        ctx.beginPath()
        if(options.color) ctx.strokeStyle = options.color
        if(options.width) ctx.lineWidth = options.width
        ctx.moveTo(from.x, from.y)
        ctx.lineTo(to.x, to.y)
        ctx.closePath()
        ctx.stroke()
        ctx.closePath()
    }
    start_stylus_listener(){
        console.log("53");
        this.#listen_stylus = true
        this.catch_stylus_pos()
    }
    stop_stylus_listener(){
        this.#listen_stylus = false
        this.#stylus_state.live = false
        this.#canvas_element.removeEventListener('touchend')
        this.#canvas_element.removeEventListener('touchstart')
        this.#canvas_element.removeEventListener('touchmove')
    }
    catch_stylus_pos(){
        var mouseDown = false
        var pv_cordinate = {x:0, y:0}
        this.#canvas_element.addEventListener('touchend', ()=>{
            mouseDown = false
        })
        this.#canvas_element.addEventListener('touchstart', (e)=>{
            mouseDown = true
            /**@type {{x:number, y:number}} */
            const { x, y } = {x:e.changedTouches[0].pageX, y:e.changedTouches[0].pageY}
            pv_cordinate = { x, y}
            window.requestAnimationFrame(()=>{
                this.draw_line({ x:x-1, y:y-1}, { x:x+1, y:y+1}, {width:5})
            })
        })
        this.#canvas_element.addEventListener('touchmove', (e)=>{
            if(!mouseDown) return
            /**@type {{x:number, y:number}} */
            const { x, y } = {x:e.changedTouches[0].pageX, y:e.changedTouches[0].pageY}
            const _pv_cordinate = pv_cordinate
            pv_cordinate = { x, y}
            window.requestAnimationFrame(()=>{
                this.draw_line(_pv_cordinate, { x, y}, {width:5})
            })
        })
    }
}


/**
 * @param {string|null} id - Element ID of canvas
 */
function canvas(id = null){
    const cv = id? document.getElementById(id): document.createElement('canvas')
    return new Canvas(cv)
}

export {canvas}