import {SVG, Line} from './svg.js'

class SVGCanvas{
    /** @type {SVG} */
    #svg
    /** @type {HTMLElement} */
    #canvas_element
    /** @type {[Line]} */
    #elememt_list = []
    /** @type {boolean} */
    #listen_stylus
    /** @type {{live:boolean, write:boolean, select:boolean}} */
    #stylus_state = {live:false, write:false, select:false}
    
    #min = {x:null, y:null}
    #max = {x:null, y:null}
    
    constructor(parent_element){
        this.#canvas_element = parent_element
        this.#svg = SVG().addTo(parent_element).size(1600, 900)
        //this.#svg.style('postion', 'fixed')
    }
    /**
     * @param {{x:number, y:number}} from 
     * @param {{x:number, y:number}} to 
     * @param {{color?:string, width?:number}} options
     */
    draw_line(from, to, options = {}){
        const line = this.#svg.line(from.x, from.y, to.x, to.y)
        .stroke(Object.assign({}, 
            options.color? {color:options.color}: null,
            options.width? {width:options.width}: null
        ))
        this.#elememt_list.push(line)
    }
    /**
     * @param {{x:number, y:number}} from
     * @param {{x:number, y:number}} to 
     */
    move_all(from, to){
        this.#elememt_list.forEach(elm=>{
            const x_distance = from.x - to.x, y_distance = from.y - to.y
            elm.dmove( -x_distance, -y_distance)
        })
    }
    start_stylus_listener(){
        console.log("53");
        this.#listen_stylus = true
        this.#stylus_state.live = true
        //this.#stylus_state.select = true
        this.#stylus_state.write = true
        this.catch_stylus_pos()
    }
    stop_stylus_listener(){
        this.#listen_stylus = false
        this.#stylus_state.live = false
        this.#canvas_element.ontouchend = null
        this.#canvas_element.ontouchstart = null
        this.#canvas_element.removeEventListener = null
    }
    catch_stylus_pos(){
        var mouseDown = false
        var pv_cordinate = {x:0, y:0}
        this.#canvas_element.addEventListener('touchend', (e)=>{
            mouseDown = false
            /**@type {{x:number, y:number}} */
            const { x, y } = {x:e.changedTouches[0].pageX, y:e.changedTouches[0].pageY}
            const _pv_cordinate = pv_cordinate
            pv_cordinate = { x, y}
            window.requestAnimationFrame(()=>{
                if(this.#stylus_state.write)
                    this.draw_line(_pv_cordinate, { x, y}, {width:5, color:'silver'})
                if(this.#stylus_state.select){
                    this.move_all(_pv_cordinate, {x, y})
                }
            })
        })
        this.#canvas_element.addEventListener('touchstart', (e)=>{
            mouseDown = true
            /**@type {{x:number, y:number}} */
            const { x, y } = {x:e.changedTouches[0].pageX, y:e.changedTouches[0].pageY}
            pv_cordinate = { x, y}
            window.requestAnimationFrame(()=>{
                console.log(this.#stylus_state);
                if(this.#stylus_state.write)
                    this.draw_line({ x:x-1, y:y-1}, { x:x+1, y:y+1}, {width:5, color:'silver'})
                if(this.#stylus_state.select){
                    //select this
                }
            })
        })
        this.#canvas_element.addEventListener('touchmove', (e)=>{
            if(!mouseDown) return
            /**@type {{x:number, y:number}} */
            const { x, y } = {x:e.changedTouches[0].pageX, y:e.changedTouches[0].pageY}
            const _pv_cordinate = pv_cordinate
            pv_cordinate = { x, y}
            window.requestAnimationFrame(()=>{
                if(this.#stylus_state.write)
                    this.draw_line(_pv_cordinate, { x, y}, {width:5, color:'silver'})
                if(this.#stylus_state.select){
                    this.move_all(_pv_cordinate, {x, y})
                }
            })
        })
    }
    write_mode(){
        this.#stylus_state.write = true
        this.#stylus_state.select = false
    }
    select_mode(){
        this.#stylus_state.write = false
        this.#stylus_state.select = true
    }
    pause_mode(){
        this.#stylus_state.write = false
        this.#stylus_state.select = false
        //this.stop_stylus_listener()
    }
}

function svg_canvas(parent_element){
    return new SVGCanvas(parent_element)
}

export {svg_canvas, SVGCanvas}