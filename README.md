[multi layer](https://developer.ibm.com/technologies/web-development/tutorials/wa-canvashtml5layering/)

### Start the server

Very basic http server, just for testing front end

```
node server.js
```

Open url [http://localhost:3000/](http://localhost:3000/)